//
//  AppDelegate.h
//  QiNiuTest
//
//  Created by 杜树忠 on 2019/11/6.
//  Copyright © 2019 杜树忠. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

