//
//  main.m
//  QiNiuTest
//
//  Created by 杜树忠 on 2019/11/6.
//  Copyright © 2019 杜树忠. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
